import React from 'react';
import ReactDOM from 'react-dom';
import './styles.css';
import TodoList from './TodoList';


ReactDOM.render(
    <div className="d-flex justify-content-center pt-4">
        <TodoList />
    </div>,
    document.querySelector('#TodoApp')
);