import React from 'react';

class TodoItems extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.createTasks = this.createTasks.bind(this);
        this.delete = this.delete.bind(this);
    }

    createTasks (item) {
        return (
        <li 
            className = "list-group-item list-group-item-action"
            onClick = { (event) => this.delete(item.key, event) } 
            key = { item.key } > 
                { item.text }
        </li>
    );
        
    }

    delete (key) {
        this.props.delete(key);
    }

    render () {
        let todoEntries = this.props.entries;
        let listItems = todoEntries.map(this.createTasks);

        return (
            <ul className = "list-group pt-2">
                { listItems }
            </ul>
        );
    }
}

export default TodoItems;