import React from 'react';
import TodoItems from './TodoItems';


class TodoList extends React.Component {

    constructor (props, context) {
        super (props, context);

        this.state = {
            items: []
        };

        this.addItem = this.addItem.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
    }

    addItem (event) {
        let itemArray = this.state.items;

        if (this._inputElement.value !== '') {
            itemArray.unshift({
                text: this._inputElement.value,
                key: Date.now()
            });

            this.setState ({
                item: itemArray
            });

            this._inputElement.value = "";

        }
        event.preventDefault();
    }

    deleteItem (key) {
        let filteredItems = this.state.items.filter((item) => {
            return (item.key !== key);
        });

        this.setState({
            items: filteredItems
        });
    }

    render () {
        return(
            <div>
                <div>
                    <form onSubmit = { this.addItem } >
                        <div className="input-group">
                            <input ref = {(a) => this._inputElement = a}
                                placeholder = "Enter Todo here.." >
                            </input>
                            <span className="input-group-btn">
                                <button type = "submit" className = "btn btn-primary">
                                    add
                                </button>
                            </span>
                        </div>
                    </form>
                </div>
                <TodoItems entries = { this.state.items } 
                    delete = { this.deleteItem } />
                <span className="text-secondary">
                    Note: Clicking on a Todo will remove it
                </span>
            </div>
        );
    }
}

export default TodoList;